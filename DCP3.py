"""

DCP3:
Given the root to a binary tree, implement serialize(root), which serializes the tree into a string, and deserialize(s), which deserializes the string back into the tree.
For example, given the following Node class
class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
The following test should pass:
node = Node('root', Node('left', Node('left.left')), Node('right'))
assert deserialize(serialize(node)).left.left.val == 'left.left'

"""

import json
 
class Node:
    def __init__(self, val, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right
 
node = Node('root', Node('left', Node('left.left')), Node('right'))
 
def put_in_dict(node):
    d = node.__dict__.copy()
    for k,v in d.items():
        if isinstance(v, Node):
            d[k] = put_in_dict(v) # recursive call
    return d
 
def serialize(node):
    d = put_in_dict(node)
    return json.dumps(d)
 
def get_from_dict(d):
    n = Node(None)
    for k,v in d.items():
        if isinstance(v, dict):
            setattr(n, k, get_from_dict(v)) # recursive call
        else:
            setattr(n, k, v)
    return n
 
def deserialize(s):
    d = json.loads(s)
    n = get_from_dict(d)
    return n
   
assert deserialize(serialize(node)).left.left.val == 'left.left'