"""

DCP2:
Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
Follow-up: what if you can't use division?

"""

from operator import mul
from itertools import combinations
from functools import reduce
 
def DCP2_with_division(an_array):
    product_all = reduce(mul, an_array)
    new_array = [product_all//i for i in an_array]
    return new_array
 
print(DCP2_with_division([1, 2, 3, 4, 5]))
print(DCP2_with_division([3, 2, 1]))
 
 
def DCP2_without_division(an_array):
    combis = combinations(an_array, len(an_array)-1)
    new_array = [reduce(mul, combi) for combi in combis]
    new_array.reverse()
    return new_array
 
print(DCP2_without_division([1, 2, 3, 4, 5]))
print(DCP2_without_division([3, 2, 1]))