"""

DCP1:
Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
Bonus: Can you do this in one pass?

"""

from itertools import combinations
 
def check_add(a_list, k):
    list_of_couples = combinations(a_list, 2)
    return k in (sum(i) for i in list_of_couples)
       
print(check_add([10,15,3,7], 17)) # True
print(check_add([10,15,3,7], 19)) # False



