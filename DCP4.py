"""

DCP4:
Given an array of integers, find the first missing positive integer in linear time and constant space.
In other words, find the lowest positive integer that does not exist in the array.
The array can contain duplicates and negative numbers as well.
For example, the input [3, 4, -1, 1] should give 2. The input [1, 2, 0] should give 3.
You can modify the input array in-place.

"""

def DCP4(a_list):
    result = sorted(i  for i in set(a_list) if i > 0)
    for i,j in enumerate(result, 1):
        if i != j:
            return i
    return i+1


assert DCP4([3, 4, -1, 1]) == 2
assert DCP4([1, 2, 0])== 3
assert DCP4([3, 4, -1, -1, 4, 5, 2, 1, 12]) == 6
assert DCP4(list(range(-10, 1_627_345)) + list(range(1_627_346, 1_700_000))) == 1627345